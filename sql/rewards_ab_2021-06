--2021 rewards sampling analysis
------------

select
name, count(*)
from rewards_actions
group by 1;

--redemptions
select
distinct guidewire_producer_code
from rewards_actions
where name = 'GIFT_CARD_REDEMPTION'
and now() - created_at::timestamp <= '6 months';

--num producers, users by segment
with crm as (
    select distinct email as email
                  , producer_code
                  , case
                        when broker_segments ~* 'whole' then 'wholesale'
                        when broker_segments ~* 'small' then 'small_retail'
        end               as segment
    from crm_agents a
             left join crm_agencies c
                       on a.agency_producer_code = c.producer_code
    where broker_segments ~* 'whole|small'
      and (offboarded = 'No'
        or offboarded is null)
)
select
segment
, count(distinct producer_code) as n_codes
, count(distinct email) as n_email
from crm
where producer_code in ()
group by 1;

--num rewards page visits by
select
producer_code
, count(distinct session_id)
from ui_events_v2_skinny
where now() - event_time::timestamp <= '6 months'
and page = '/rewards' and input ~* 'page_enter'
group by 1

--email open rate
select count(*) from (
select
    email_address
     , count(distinct sent_at) filter ( where num_clicks > 0 ) /
       count(distinct sent_at)::float as open_rate
from mandrill_emails
group by email_address)a;

select count(*) from (
select
    email_address as email
     , count(distinct sent_at) filter ( where num_opens > 0 ) num_opened
     , count(distinct sent_at) filter ( where num_clicks > 0 ) num_clicked
     , count(distinct sent_at) filter ( where status = 'sent' ) num_sent
from mandrill_emails
group by email_address
)a;

----previous AB test analysis
------------------
with amp as (
    select
        user_id
         , event_time as opt_in_at
    from ui_events_v2_skinny
    where attune_user is false
      and event_time >= '2019-06-01'
      and input = 'rewards_opt_in'
)

   , crm as (
    select
        distinct email as crm_email
           , agency_id
           , agency_producer_code
           , onboarded_date
           , offboarded
           , case when broker_segments ~* 'whole' then 'wholesale'
                when broker_segments ~* 'small' then 'small_retail'
        end as segment
    from crm_agents a
        left join crm_agencies c
            on a.agency_producer_code = c.producer_code
    where onboarded_date >= '2019-01-01'
        and broker_segments ~* 'whole|small'
        and (offboarded = 'No'
            or offboarded is null)
)

select
       avg(ngift)
, avg(ngift_opt_in)
from (
select
guidewire_user_name
, count(distinct uuid) filter ( where name~*'gift' ) as ngift
, count(distinct uuid) filter (
    where name~*'gift' and guidewire_user_name in (select crm_email from crm)
    ) as ngift_opt_in
from rewards_actions
group by 1
    ) a;

select
count(distinct guidewire_user_name)
,count(distinct guidewire_user_name) filter ( where name~*'gift' )
from rewards_actions;

select
distinct name from rewards_actions;


---
with amp as (
    select
        user_id
         , event_time as opt_in_at
    from ui_events_v2_skinny
    where attune_user is false
      and event_time >= '2019-06-01'
      and input = 'rewards_opt_in'
)

   , crm as (
    select
        distinct email as crm_email
           , agency_id
           , agency_producer_code
           , onboarded_date
           , offboarded
           , case when broker_segments ~* 'whole' then 'wholesale'
                when broker_segments ~* 'small' then 'small_retail'
        end as segment
    from crm_agents a
        left join crm_agencies c
            on a.agency_producer_code = c.producer_code
    where onboarded_date >= '2019-01-01'
        and broker_segments ~* 'whole|small'
        and (offboarded = 'No'
            or offboarded is null)
)

, sum_week as (
    select
        date_trunc('week', bind_date_bop) as week
--         date_trunc('month', bop_quote_date) as month
         , creator_email as email
         , case when broker_segments ~* 'whole' then 'wholesale'
                when broker_segments ~* 'small' then 'small_retail'
                else trim(both '[]"]' from broker_segments)
        end as segment
--          , sum(num_quotes_bop) as n_quotes
         , sum(num_binds_bop) as n_binds
    from bi.quote_windows_extended
    where bind_date_bop >= '2019-01-01'::timestamp
      and broker_segments ~* 'whole|small'
    group by 1, 2, 3
    )


select
count(distinct a.user_id)
, count(distinct c.crm_email)
, count(distinct s.email)
-- a.user_id
-- , a.opt_in_at
-- , s.segment
-- , s.week
-- , s.n_binds
from amp a
    join crm c on a.user_id = c.crm_email
    left join sum_week s on c.crm_email = s.email
-- order by a.user_id, s.week asc;



with sum_month as (
    select
        date_trunc('month', bind_date_bop) as month
--         date_trunc('month', bop_quote_date) as month
         , creator_email as email
         , case when broker_segments ~* 'whole' then 'wholesale'
                when broker_segments ~* 'small' then 'small_retail'
                else trim(both '[]"]' from broker_segments)
        end as segment
--          , sum(num_quotes_bop) as n_quotes
         , sum(num_binds_bop) as n_binds
    from bi.quote_windows_extended
    where bop_quote_date >= '2019-06-01'::timestamp
      and broker_segments ~* 'whole|small'
    group by 1, 2, 3)

select
    month
     , segment
     , avg(n_binds) avg_n_binds_per_user
from sum_month
group by 1, 2
order by 2,1 asc;


-- select
--     count(*)
--      , count(distinct crm_email) n_crm_users
--      , count(distinct optin_email) n_optins
--      , 100 * count(distinct optin_email) /
--        count(distinct crm_email)::float as pct_optin
--      , 100 * count(distinct optin_email) filter
--     ( where date_part('year', transaction_created_at)='2020') /
--        count(distinct optin_email)::float as pct_2020_reward
-- from rewards;

---stats
with all_users as (
    select
        distinct email as crm_email
           , agency_id
           , agency_producer_code
           , onboarded_date
           , offboarded
           , trim(both '[]"]' from c.broker_segments) as segment
    from crm_agents a
        left join crm_agencies c
            on a.agency_producer_code = c.producer_code
)

   , rewards as (
    select
        agency_id
         , agency_producer_code
         , onboarded_date
         , u.crm_email
         , o.guidewire_user_name optin_email
         , t.created_at as transaction_created_at,
        t.reward_uuid
         , t.points
         , t.uuid as transaction_uuid
    from all_users u
         left join rewards_optins o
            on u.crm_email = o.guidewire_user_name
         left join rewards_transactions t
            on o.guidewire_user_name = t.guidewire_user_name
    where u.offboarded = 'No'
       or u.offboarded is null
)


select
    count(*)
     , count(distinct crm_email) n_crm_users
     , count(distinct optin_email) n_optins
     , 100 * count(distinct optin_email) /
       count(distinct crm_email)::float as pct_optin
     , 100 * count(distinct optin_email) filter
    ( where date_part('year', transaction_created_at)='2020') /
       count(distinct optin_email)::float as pct_2020_reward
from rewards;

----nquotes/binds for each test group
with sum_month as (
    select
        date_trunc('month', bind_date_bop) as month
--         date_trunc('month', bop_quote_date) as month
         , creator_email as email
         , case when broker_segments ~* 'whole' then 'wholesale'
                when broker_segments ~* 'small' then 'small_retail'
        end as segment
--          , trim(both '[]"]' from broker_segments) as segment
--          , sum(num_quotes_bop) as n_quotes
         , sum(num_binds_bop) as n_binds
    from bi.quote_windows_extended
    where bop_quote_date >= '2019-06-01'::timestamp
      and broker_segments ~* 'whole|small'
    group by 1, 2, 3)

select
    month
     , segment
     , avg(n_binds) avg_n_binds_per_user
from sum_month
group by 1, 2
order by 2,1 asc;

--reduced
with all_users as (
    select
        distinct email as crm_email
               , onboarded_date
               , offboarded
               , case when broker_segments ~* 'whole' then 'wholesale'
                      when broker_segments ~* 'small' then 'small_retail'
        end as segment
    from crm_agents a
        left join crm_agencies c
            on a.agency_producer_code = c.producer_code
    where onboarded_date between '2019-06-01' and '2020-06-01'
      and broker_segments ~* 'whole|small'
)

   , rewards as (
    select
        onboarded_date
         , u.crm_email
         , u.segment
         , o.guidewire_user_name optin_email
    from all_users u
        left join rewards_optins o
                on u.crm_email = o.guidewire_user_name
    where u.offboarded = 'No'
       or u.offboarded is null
)

select
    count(*)
     , count(distinct crm_email) n_crm_users
     , count(distinct optin_email) n_optins
     , 100 * count(distinct optin_email) /
       count(distinct crm_email)::float as pct_optin
     , count(distinct optin_email) filter ( where segment='wholesale' ) as n_optin_wholesale
     , count(distinct optin_email) filter ( where segment='small_retail' ) as n_optin_sr
from rewards;



---user optins, onboarded 6/19 - 6/20-
with all_users as (
    select
        distinct email as crm_email
           , agency_id
           , agency_producer_code
           , onboarded_date
           , offboarded
           , case when broker_segments ~* 'whole' then 'wholesale'
                when broker_segments ~* 'small' then 'small_retail'
        end as segment
    from crm_agents a
        left join crm_agencies c
            on a.agency_producer_code = c.producer_code
    where onboarded_date between '2019-06-01' and '2020-06-01'
        and broker_segments ~* 'whole|small'
)

   , rewards as (
    select
         onboarded_date
         , u.crm_email
         , u.segment
         , o.guidewire_user_name optin_email
         , case when o.guidewire_user_name is not null then true
            else false end as optin
    from all_users u
         left join rewards_optins o
            on u.crm_email = o.guidewire_user_name
    where u.offboarded = 'No'
       or u.offboarded is null
)

   , sum_month as (
    select
        date_trunc('month', bind_date_bop) as month
--         date_trunc('month', bop_quote_date) as month
         , creator_email as email
         , case when broker_segments ~* 'whole' then 'wholesale'
                when broker_segments ~* 'small' then 'small_retail'
           end as segment
         , case when creator_email in
                (select optin_email from rewards
                    where optin is true) then true
                else false
           end as optin
--          , sum(num_quotes_bop) as n_quotes
         , sum(num_binds_bop) as n_binds
    from bi.quote_windows_extended
--     where bop_quote_date >= '2020-07-01'::timestamp
      where bind_date_bop >= '2020-07-01'::timestamp
      and broker_segments ~* 'whole|small'
    group by 1, 2, 3, 4)

select
    month
     , segment
     , optin
     , avg(n_binds) avg_n_binds_per_user
--      , avg(n_quotes) avg_n_quotes_per_user
from sum_month
group by 1, 2, 3
order by 2,3,1 asc;


with sum_month as (
    select
         date_trunc('month', bop_quote_date) as month
         , creator_email as email
         , case when broker_segments ~* 'whole' then 'wholesale'
                when broker_segments ~* 'small' then 'small_retail'
           end as segment
         , sum(num_quotes_bop) as n_quotes
    from bi.quote_windows_extended
    where bop_quote_date >= '2020-01-01'::timestamp
      and broker_segments ~* 'whole|small'
    group by 1, 2, 3)

select
    month
     , segment
     , avg(n_quotes) avg_n_quotes_per_user
from sum_month
group by 1, 2
order by 2,1 asc;

---stats
with agents as (
    select distinct email
                  , trim(both '[]"]' from c.broker_segments) as segment
    from crm_agents a
             left join crm_agencies c
                       on a.agency_producer_code = c.producer_code
    where (offboarded = 'No' or offboarded is null)
)

   , rewards as (
    select
         u.segment
         , u.email
         , o.guidewire_user_name optin_email
         , t.created_at as transaction_created_at,
        t.reward_uuid
         , t.points
         , t.uuid as transaction_uuid
    from agents u
         left join rewards_optins o
            on u.email = o.guidewire_user_name
         left join rewards_transactions t
            on o.guidewire_user_name = t.guidewire_user_name
)


select
    count(*)
     , count(distinct crm_email) n_crm_users
     , count(distinct optin_email) n_optins
     , 100 * count(distinct optin_email) /
       count(distinct crm_email)::float as pct_optin
     , 100 * count(distinct optin_email) filter
    ( where date_part('year', transaction_created_at)='2020') /
       count(distinct optin_email)::float as pct_2020_reward
from rewards;

----AB TEST Tracking Dashboard Development
-----
-----email tracking
with agents as (
    select distinct email
      , trim(both '[]"]' from c.broker_segments) as segment
    from crm_agents a
        left join crm_agencies c
            on a.agency_producer_code = c.producer_code
      where (offboarded = 'No' or offboarded is null)
)

, mandrill as (
    select m.*
     , case
           when a.segment ~* 'small' then 'small_retail'
           else lower(a.segment) end as segment
    from mandrill_emails m
        left join agents a
            on m.email_address = a.email
    where email_address in (
        select email
        from personal.src_rewards_ab_groups_06_2021
        where test_group = 'treatment'
    )
      and tags ~* 'reward'
      and status = 'sent'
      and m.sent_at >= '2021-05-21'
)


select
    date_trunc('day', sent_at) as day
     , segment
     , 'treatment' as test_group
     , count(*) as emails_sent
     , count(*) filter ( where num_clicks > 0) as emails_clicked
-- , count(*) filter ( where now() - sent_at <= '7 days') as sent_7days
-- , count(*) filter ( where num_clicks > 0 and now() - sent_at <= '7 days') as clicked_7days
-- , count(*) filter ( where sent_at >= '2021-05-21') as sent_exp_start
-- , count(*) filter ( where num_clicks > 0 and sent_at >= '2021-05-21') as clicked_exp_start
from mandrill
where segment ~* 'small|whole'
group by 1,2,3;

--------
-----redemptions

with agents as (
    select distinct email
        ,trim(both '[]"]' from c.broker_segments) as segment
    from crm_agents a
        left join crm_agencies c
            on a.agency_producer_code = c.producer_code
    where (offboarded = 'No' or offboarded is null)
    and c.territories != '["Florida"]'
)
   , rewards as (
    select
        case when segment ~* 'whole' then 'wholesale'
             when segment ~* 'small' then 'small_retail'
            end as segment
         , u.email
         , t.created_at as transaction_at
         , t.points
         , r.name
         , p.test_group
    from agents u
        left join rewards_transactions t
            on u.email = t.guidewire_user_name
        left join rewards_actions r
            on t.action_uuid = r.uuid
        left join personal.src_rewards_ab_groups_06_2021 p
             on u.email = p.email
    where t.created_at >= '2021-05-21'
)

select
    date_trunc('day', transaction_at) as day
    ,test_group
     , segment
     , count(distinct email) as redemptions
--      , count(distinct email) filter ( where now() - transaction_at <= '7 days') as redemptions_7days
--      , count(distinct email) filter ( where transaction_at >= '2021-05-21') as redemptions_exp_start
from rewards
where test_group is not null
  and name is not null
  and segment in ('small_retail', 'wholesale')
group by 1,2,3;


---page visits
-------
with agents as (
    select distinct email
        ,trim(both '[]"]' from c.broker_segments) as segment
    from crm_agents a
        left join crm_agencies c
            on a.agency_producer_code = c.producer_code
    where (offboarded = 'No' or offboarded is null)
    and c.territories != '["Florida"]'
)
,
 amp as (
     select
        case when segment ~* 'whole' then 'wholesale'
             when segment ~* 'small' then 'small_retail'
            end as segment
         , u.email
         , ui.event_time
         , p.test_group
    from agents u
    left join ui_events_v2_skinny ui
        on u.email = ui.user_id
    right join personal.src_rewards_ab_groups_06_2021 p
             on ui.user_id = p.email
    where attune_user is false
      and page ~* 'reward'
      and input ~* 'page_enter'
      and event_time >= '2021-05-21'
)

select
    date_trunc('day', event_time) as day
     ,test_group
     , segment
     , count(distinct email) as page_visits
-- , count(distinct email) filter ( where now() - event_time <= '7 days') as page_visits_7days
-- , count(distinct email) filter ( where event_time >= '2021-05-21') as page_visits_exp_start
from amp
where segment in ('small_retail', 'wholesale')
group by 1,2,3;

select
count(*)
, count(*) filter ( where num_opens > 0 )
, count(*) filter ( where num_clicks > 0 )
from mandrill_emails
where tags ~* 'reward'


---testing
with agents as (
          select distinct email
            , trim(both '[]"]' from c.broker_segments) as segment
          from crm_agents a
              left join crm_agencies c
                  on a.agency_producer_code = c.producer_code
            where (offboarded = 'No' or offboarded is null)
            and c.territories != '["Florida"]'
      )

      , mandrill as (
          select m.*
           , case
                 when a.segment ~* 'small' then 'small_retail'
                 else lower(a.segment) end as segment
          from mandrill_emails m
              left join agents a
                  on m.email_address = a.email
          where email_address in (
              select email
              from personal.src_rewards_ab_groups_06_2021
              where test_group = 'treatment'
          )
            and tags ~* 'reward'
            and status = 'sent'
            and sent_at >= '2021-05-21'
      )


      select
          date_trunc('week', sent_at) as week
           , segment
           , 'treatment' as test_group
           , count(distinct (sent_at, email_address)) as emails_sent
           , count(distinct (sent_at, email_address)) filter ( where num_clicks > 0) as emails_clicked
      from mandrill
      where segment in ('small_retail', 'wholesale')
      group by 1,2,3;

--Treatment users NOT in okta
select
count(distinct login)
from okta_users
where status not in ('DEPROVISIONED','SUSPENDED')
and login not in (
    select email
    from personal.src_rewards_ab_groups_06_2021
    where test_group = 'treatment'
    )

-- Scratch
with agent as (
    select *
    from crm_agents
    where email = 'johnj@wesmckenna.com'
)
select
*
from agent a
left join crm_agencies c on a.agency_id=c.parent_agency_id;