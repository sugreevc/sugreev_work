import pandas as pd
import numpy as np
import pyspark
import sparknlp
import databricks.koalas as ks

from sparknlp.pretrained import PretrainedPipeline
from sparknlp import Finisher
from sparknlp.base import DocumentAssembler
from sparknlp.annotator import (
    SentenceDetector,
    Tokenizer,
    Normalizer,
    LemmatizerModel,
    StopWordsCleaner,
)

from sklearn.feature_extraction.text import ENGLISH_STOP_WORDS

from pyspark.ml import Pipeline
from pyspark.ml.feature import HashingTF, IDF, CountVectorizer
from pyspark.ml.clustering import LDA

from pyspark.sql import functions as F
from pyspark.sql import types as T

# Start session
spark = sparknlp.start()

# Dataframe schema is encoded in string.
schemaString = "ticket_id text"
fields = [
    T.StructField(field_name, T.StringType(), True)
    for field_name in schemaString.split()
]
schema = T.StructType(fields)

# Read data from CSV
data_path = "../data/ticket_comments_2020-09.csv"
df = spark.read.csv(path=data_path, header=True, multiLine=True, schema=schema)

# Test data
sentences = [["Hello, this is an example sentence"], ["And this is a second sentence."]]
test_data = spark.createDataFrame(sentences).toDF("text")

# Define pipeline stages
document_assembler = (
    DocumentAssembler().setInputCol("text").setOutputCol("document")
)  # .setIdCol('ticket_id')
sentenceDetector = (
    SentenceDetector().setInputCols(["document"]).setOutputCol("sentences")
)
tokenizer = Tokenizer().setInputCols(["document"]).setOutputCol("token")
normalizer = (
    Normalizer().setInputCols(["token"]).setOutputCol("normal").setLowercase(True)
)
lemmatizer = LemmatizerModel.pretrained().setInputCols(["normal"]).setOutputCol("lemma")
stopwords = (
    StopWordsCleaner()
    .setInputCols(["lemma"])
    .setOutputCol("stop_lemmatized")
    .setStopWords(list(ENGLISH_STOP_WORDS))
    .setCaseSensitive(False)
)
finisher = Finisher().setInputCols(["stop_lemmatized"])
tf = CountVectorizer(
    minDF=1.0,
    maxDF=0.6,
    vocabSize=1000,
    inputCol="finished_stop_lemmatized",
    outputCol="tf",
)
idf = IDF(minDocFreq=5, inputCol="tf", outputCol="tfidf")
lda = LDA(featuresCol="tfidf", maxIter=20, k=10)

# Define pipeline
pipeline = Pipeline(
    stages=[
        document_assembler,
        tokenizer,
        normalizer,
        lemmatizer,
        stopwords,
        finisher,
        tf,
        idf,
        lda,
    ]
)

# Fit & transform
pipeline_fit = pipeline.fit(test_data)
annotated_df = pipeline_fit.transform(test_data)

# Get models from pipeline
lda_model = pipeline_fit.stages[-1]
tf_model = pipeline_fit.stages[-3]


# Define Spark UDF
vocab = tf_model.vocabulary


def get_words(token_list):
    return [vocab[token_id] for token_id in token_list]


udf_to_words = F.udf(get_words, T.ArrayType(T.StringType()))

# Extract topics
num_top_words = 10
topics = lda_model.describeTopics(maxTermsPerTopic=num_top_words).withColumn(
    "topicWords", udf_to_words(F.col("termIndices"))
)
topics.show(truncate=False)
