import re
import string
from collections import namedtuple, OrderedDict

url_pattern = (
    r"http[s]?://(?:[a-zA-Z]|[0-9]|[$-_@.&+]|[!*\(\),]|(?:%[0-9a-fA-F][0-9a-fA-F]))+"
)
email_pattern = r"[a-z0-9]+[\._]?[a-z0-9]+[@]\w+[.]\w{2,3}"
punct_pattern = r"[!\"#$%&\'()*+,./:;=?@\[\\\]^`{}~]+"

TokenSub = namedtuple("TokenSub", ["pattern", "repl"])

token_specifications = OrderedDict(
    {
        "GREETING": TokenSub(
            r"(dear|hi|hello|hey|good ?(afternoon|morning|evening|day)?)( \w+)?[,!]*\n+",
            "",
        ),
        "PHONE_SIGNATURE": TokenSub(
            r"Sent (from|via).+(?=(i?phone|windows|TMobile|att|[45]g lte|spring|samsung|galaxy))(.|\n)*",
            "",
        ),
        "ATTUNE_FOOTER": TokenSub(
            r"((sincerely|kind regards),\n+)?\w+\n+attune customer care(.|\n)*", ""
        ),
        "BROKER_FOOTER": TokenSub(
            r"(sincerely|thanks?( you)?|(kind|warm )?regards).{0,15}[.,!]*\n+(.|\n)+",
            "",
        ),
        "SIGNATURE": TokenSub(
            r"(\n+[\w\|:.\(\), ]{1,40}\n+)(?=([\w\|@:.\(\)\/\[\], ]{5,75}\n+){3,7})(.|\n)*",
            "",
        ),
        "CONFIDENTIALITY_FOOTER": TokenSub(
            r"This email and any(.|\n)+(sender by return|transmitted by this) email", ""
        ),
        "ATTUNE_SUBMITTED_FROM": TokenSub(r"Submitted from: https://.*attune.+", ""),
        "EMAIL_REPLY": TokenSub(
            r"from:.+\n+((sent|to|cc|date|subject):.+\n*)+(.|\n)*", ""
        ),
        "NEWLINES": TokenSub(r"(\s?\n\s?)+", ""),
        "ACCT_PH": TokenSub(r"\d{10}", "<ACCT_PH>"),
        "POLICY": TokenSub(r"[a-zA-Z0-9]{11,28}\d{7}", "<POLICY>"),
        "EMAIL": TokenSub(email_pattern, "<EMAIL>"),
        "URL": TokenSub(r"[a-zA-Z0-9]{18,35}", ""),
    }
)


def repl(match_object, token_specs, replace=True):
    """
    Given a match_object obtained from re.search(token_regex, string)
    where token_regex is built from token_specs, return a replacement pattern
    corresponding to the token type found (if this method is used in a re.sub 
    expression for text preprocessing, for example) or return the token_type found
    if being used for exploration
    
    Args:
        match_object (re.match object): from token_regex match on some string
        token_specs (dict(string, TokenSub)): specifies token name, pattern, repl
        replace (bool, optional): default True. Returns repl pattern if True else
            <token_type>

    Returns:
        string, either repl or token_type

    
    """
    token_type = match_object.lastgroup
    if replace:
        return token_specs[token_type].repl
    return f"<{token_type}>"


def make_token_regex(token_specs):
    token_regex = "|".join(
        [
            f"(?P<{token_type}>{tokensub.pattern})"
            for token_type, tokensub in token_specs.items()
        ]
    )
    return token_regex


def preprocess(text: str, token_specs=token_specifications, replace=True):
    text = text.encode("ascii", errors="ignore").decode()
    text = text.lower()
    text = re.sub(r"[\|_*&<>\-]+", "", text)  # special characters
    text = re.sub(r" +", " ", text)  # excess whitespace
    text = re.sub(r"((\r| )+\n|\n(\r| )+)", "\n", text)  # whitespace-newline
    text = re.sub(
        make_token_regex(token_specs),
        lambda x: repl(x, token_specs, replace=replace),
        text,
        flags=re.IGNORECASE,
    )
    text = re.sub(punct_pattern, " ", text)  # punctuation
    text = re.sub(r"\d+", " ", text)  # numbers
    text = " ".join(text.strip().split())  # excess whitespace
    return text
