"""
Clean and preprocess ticket comment text data.
"""

import re
from collections import OrderedDict
import pandas as pd
import numpy as np


class ChatTextFeatures(object):

    """Process zendesk ticket chat text and extract features"""

    # specific to chat tickets eg, "(10:45:55 PM) Amit K.:""
    TIMESTAMP_NAME_PATTERN = r"\((.+:.+:.+ [AP]M)\) (.+?):"

    def __init__(self, text, t_created):
        self.text = text.strip()
        self.t_created = t_created  # string 2019-11-09 10:45:55
        self.text_type = self.get_text_type()
        self.broker_name = None
        self.agent_name = None
        self.parsed_chat = self.parse_chats()
        self.num_parsing_errors = self.calc_num_error_lines()
        self.timestamp_features = self.extract_timestamp_features()
        self.tsf_error = self.calc_tsf_error()

    def get_text_type(self):
        """Labels text based on presence of timestamp. """

        return "chat" if re.search(self.TIMESTAMP_NAME_PATTERN, self.text) else "macro"

    def parse_chats(self):
        """Separates ticket chat text into list of chat dicts with keys timestamp, name,
        chat message and label of broker vs agent.

        Returns:
            list(dict): returns a list of lists of customer and agent chat lines

        """
        if self.text_type == "chat":
            parsed_chat = []
            # remove empty lines
            lines = [i for i in self.text.split("\n") if i != "\n"]
            anchor_index = 0
            for line in lines:
                parsed_line = OrderedDict().fromkeys(
                    {"name", "timestamp", "text", "author"}
                )
                search = re.search(self.TIMESTAMP_NAME_PATTERN, line)
                if search:
                    parsed_line["name"] = search.group(2)
                    parsed_line["timestamp"] = self.to_timestamp(search.group(1))
                    parsed_line["text"] = line.replace(search.group(0), "").strip()

                    if not parsed_chat or not self.broker_name:
                        self.broker_name = parsed_line["name"]
                        parsed_line["author"] = "broker"
                    elif parsed_line["name"] == self.broker_name:
                        parsed_line["author"] = "broker"
                    else:
                        parsed_line["author"] = "agent"
                        if not self.agent_name:
                            self.agent_name = parsed_line["name"]

                    parsed_chat.append(parsed_line)
                    anchor_index = len(parsed_chat) - 1
                else:
                    # if a timestamp pattern not found,
                    # assume line is text continued from anchor line
                    # ignore first line errors
                    if anchor_index > 1:
                        parsed_chat[anchor_index]["text"] = " ".join(
                            [parsed_chat[anchor_index]["text"], line]
                        )

            return parsed_chat
        else:
            return None

    def calc_num_error_lines(self):
        """Get num lines from self.parsed_chat that have None values

        Returns:
            int: num lines
        """

        num_errors = 0
        if self.text_type == "chat":
            for parsed_line in self.parsed_chat:
                if None in parsed_line.values():
                    num_errors += 1
        return num_errors

    def calc_tsf_error(self):
        """Bool indicating if None in dict values of self.timestamp_features, for chats only

        Returns:
            int: num lines
        """

        if self.text_type == "chat":
            return any([None in self.timestamp_features.values()])
        else:
            return None

    def get_chat_component(self, component, author=None):
        """Get component of chat lines from self.parsed_chat
        
        Args:
            component (str): field of parsed chat line (eg 'name' or 'timestamp')
            author (str, optional): 'broker' filters chat lines to broker chats, 'agent' for agent.
                Default None (return all).

        Returns:
            list(str or pandas Timestamp) 
        """
        if self.parsed_chat:
            if author:
                return [
                    parsed_line[component]
                    for parsed_line in self.parsed_chat
                    if parsed_line["author"] == author
                ]
            else:
                return [parsed_line[component] for parsed_line in self.parsed_chat]
        else:
            return None

    def extract_timestamp_features(self):
        """Extracts features from timestamps of ticket chat text.

        Returns:
            dict: extracted features
        """
        features = OrderedDict.fromkeys(
            {"total", "rounded_total", "diffs", "max_diffs", "mean_diffs"}
        )

        agent_timestamps = self.get_chat_component("timestamp", author="agent")
        # filter Nones for now
        agent_timestamps = self.remove_nones(agent_timestamps)
        if agent_timestamps:
            features["total"] = self.calc_total_time(agent_timestamps)
            features["rounded_total"] = round(features["total"])

        all_timestamps = self.get_chat_component("timestamp")
        all_timestamps = self.remove_nones(all_timestamps)
        if all_timestamps:

            features["diffs"] = self.calc_timestamp_diffs(all_timestamps)
            features["max_diffs"] = np.max(features["diffs"])
            features["mean_diffs"] = np.mean(features["diffs"])

        return features

    def to_timestamp(self, timestamp_string):
        """make pandas timestamp from timestamp strings"""

        try:
            return pd.to_datetime(
                " ".join(
                    [self.t_created, timestamp_string.replace("(", "").replace(")", "")]
                )
            )
        except Exception:
            return None

    @staticmethod
    def find_changepoint_indices(input_list):
        """Finds indices of list where value changes from previous.

        Returns:
            list: indices
        """
        change_inds = []
        for i, val in enumerate(input_list):
            if i > 0:
                if val != input_list[i - 1]:
                    change_inds.append(i)
        return change_inds

    @staticmethod
    def remove_nones(input_list):
        """remove Nones from list"""
        return [i for i in input_list if i] if input_list else None

    @staticmethod
    def calc_total_time(timestamp_list):
        """Time between first and last timestamp from list of sequential timestamps
        
        Args:
            timestamp_list (list(pandas Timestamp)): extracted from chat text

        Returns:
            float: time in minutes

        """
        if len(timestamp_list) == 1:
            return 0.1  # assume 1 line answer from agent takes 6s
        else:
            return (timestamp_list[-1] - timestamp_list[0]).total_seconds() / 60

    @staticmethod
    def calc_timestamp_diffs(timestamp_list):
        """Differences between sequential timestamps
        
        Args:
            timestamp_list (list(pandas Timestamp)): extracted from chat text

        Returns:
            list(float): times in minutes

        """

        return [d.total_seconds() / 60 for d in np.diff(timestamp_list)]
