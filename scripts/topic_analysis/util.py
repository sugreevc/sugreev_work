import datetime
from typing import List

import numpy as np
from functools import wraps
from time import time


# TFIDF inspection
def inspect_tfidf(fit_tfidf_vectorizer, vectorized_data):
    """
    Return all feature names and set of top 5 features for each doc

    Args:
        fit_tfidf_vectorizer (sklearn vectorizer): after executing vectorizer.fit()
        vectorized_data (numpy sparse matrix): result of vectorizer.transform()

    Returns:
        dict of
            feature_array (numpy array): all features from fit_tfidf_vectorizer.get_feature_names
            top_features (numpy array): get top features for each doc and return flattened unique elements
    """

    feature_array = np.array(fit_tfidf_vectorizer.get_feature_names())

    # top 5 most important words for every doc
    tfidf_sorting = np.ravel(
        np.fliplr(np.argsort(tfv.toarray(), axis=1))[:, :5], order="F"
    )
    u, ind, cnt = np.unique(
        feature_array[tfidf_sorting], return_index=True, return_counts=True
    )

    return {
        "feature_names": feature_array,
        "top_features_counts": np.array(list(zip(u, cnt)))[np.argsort(ind)],
        "top_features": u[np.argsort(ind)],
    }


def find_docs_with_word(word, tfidf_vectorizer, vectorized_data, data, highlight=False):
    """
    Get doc indices of data matrix (n docs x n features) that contain
    nonzero feature weight for 'word'

    Useful for building stopwords lists; if a word appears in a topic
    or is highly weighted according to TFIDF, apply this function to find
    docs containing the word so you can manually decide importance.

    Args:
        word (str): word (decoded tfidf feature) of interest
        fit_tfidf_vectorizer (sklearn vectorizer): after executing vectorizer.fit()
        vectorized_data (numpy sparse matrix): result of vectorizer.transform()
        data (numpy array): text data fit_transformed by vectorizer, n docs x 1
        highlight (optional, bool): True if word should be highlighted when printed
            with ANSI escape chars

    Returns:
        doc_inds (numpy array)
    """

    feature_index = tfidf_vectorizer.vocabulary_[word]
    doc_inds = vectorized_data[:, feature_index].toarray().squeeze().nonzero()[0]
    if highlight:
        return np.array(list(map(lambda x: highlight_word(x, word), data[doc_inds])))
    return data[doc_inds]


def get_top_keywords_per_topic(fit_vectorizer, trained_model, top_n=3):
    """
    Get top_n keywords per topic by finding indices of higest weighted features,
    then decoding those features back to tokens using the vectorizer.

    Args:
        fit_vectorizer (sklearn vectorizer): after executing vectorizer.fit()
        trained_model (sklearn model): after executing model.fit()

    Returns:
        top_keywords_per_topic(numpy matrix): t topics x top_n keywords

    """
    feature_tokens = fit_vectorizer.get_feature_names()
    top_keywords_per_topic = np.apply_along_axis(
        lambda x: [feature_tokens[i] for i in x],
        axis=1,
        arr=np.fliplr(np.argsort(trained_model.components_, axis=1))[:, :top_n],
    )
    return top_keywords_per_topic


def get_top_topics_per_doc(model_output, top_n=3):
    """Get top_n n topics and weights per document from data transformed by LDA topic model.
    
    Args:
        model_output (numpy matrix): m samples x n topic weights, 
            from lda model .transform() output
        top_n (int, optional): num topics per document. Defaults to 3.

    Returns:
        top_topic_inds (numpy matrix): m samples x top_n (topic number)
        top_topic_weights (numpy matrix): m samples x top_n (topic weight)
    """

    top_topic_inds = np.fliplr(np.argsort(model_output, axis=1))[:, :top_n].squeeze()
    top_topic_weights = np.fliplr(np.sort(model_output, axis=1))[:, :top_n].squeeze()

    return {"top_topic_inds": top_topic_inds, "top_topic_weights": top_topic_weights}


def topic_weight_histogram(topic_weights_per_doc):
    """
    Given an array of size (m x 1), where m is the number of samples/docs 
    and the array values are topic weights for a particular topic number,
    output a histogram of the number of docs per bin.

    """
    arr = topic_weights_per_doc.round(1).squeeze()
    bins = [0, 0.7, 0.8, 0.9, 1]
    docs_per_bin, _ = np.histogram(arr, bins=bins)
    return docs_per_bin


def get_topic_coverage_stats(model_output, top_n=3):
    """
    Restrict model output to top n topics of interest, take cumulative sum 
    to find the total weight covered by the 1st, 1st and 2nd, ... sum(1-Nth) topic weights per doc.

    Args:
        model_output (numpy matrix): m samples x n topic weights, 
            from lda model .transform() output

    Returns:
        top_n_tfidf_features(numpy matrix): m samples x n feature tokens
        top_n_tfidf_weights (numpy matrix): m samples x n feature weights

    """

    # this is a matrix of size (N top topics x n_bin_edges), values are sample counts per bin
    weight_hist_by_topic_N = np.apply_along_axis(
        topic_weight_histogram,
        axis=0,
        arr=np.fliplr(np.sort(model_output, axis=1)).cumsum(axis=1)[:, :top_n],
    ).T

    pct = weight_hist_by_topic_N / weight_hist_by_topic_N.sum(axis=1, keepdims=True)
    return {
        "weight_hist_by_topic_N": weight_hist_by_topic_N,
        "weight_hist_by_topic_N_pct": pct,
    }


def highlight_word(text, word):
    return text.lower().replace(word, "\x1b[1;03;30;43m" + word + "\x1b[0m")
