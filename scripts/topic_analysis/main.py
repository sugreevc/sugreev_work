import datetime
from pathlib import Path

import pandas as pd
import numpy as np

import mlflow
import mlflow.sklearn
from mlflow.tracking import MlflowClient

from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.decomposition import LatentDirichletAllocation
from sklearn.pipeline import Pipeline

from stopwords import get_stopwords
from util import get_top_keywords_per_topic, get_top_topics_per_doc
from email_preprocess import preprocess


if __name__ == "__main__":

    # Set experiment
    experiment_name = "eligibility_tickets"
    mlflow.set_experiment(experiment_name)
    client = MlflowClient()
    experiment_id = client.get_experiment_by_name(experiment_name).experiment_id

    # Set MLFlow tags
    framework = "sklearn"
    preprocessing_version = "v2"
    stopwords_version = "v2"

    stop_words = get_stopwords()

    # Get data
    PROJECT_ROOT = Path(
        "/Users/sugreevchawla/Desktop/repos/sugreev_work/scripts/topic_analysis"
    )
    # data_path = PROJECT_ROOT / "data" / "ticket_comments_2020-09.csv"
    # data_path = PROJECT_ROOT / "data" / "ticket_comments_billing_r2.csv"
    data_path = PROJECT_ROOT / "data" / "eligibility_email_tix.csv"

    # Define output filenames
    KEYWORDS_PER_TOPIC_FILE = "topics.csv"
    DATAFRAME_FILE = "dataframe.csv"
    TFIDF_FEATURES_FILE = "tfidf_features.csv"

    # for n_rows in (5000, 10000, 20000, 50000):

    with mlflow.start_run(experiment_id=experiment_id) as run:

        print(f"Started MLflow run, experiment {experiment_id}, run {run.info.run_id}")

        # Load Data
        tic = datetime.datetime.now()
        df = pd.read_csv(
            filepath_or_buffer=data_path,
            names=("ticket_id", "created", "author", "aid", "doc", "channel", "rank"),
            lineterminator="\n",
        )  # .head(n_rows)
        toc = datetime.datetime.now()
        load_time = (toc - tic).seconds
        print(f"Data Loaded in {load_time} seconds...")

        # Setup pipeline
        pipeline = Pipeline(
            [("tfidf", TfidfVectorizer()), ("lda", LatentDirichletAllocation())]
        )

        pipeline_params = {
            "tfidf__strip_accents": "unicode",
            "tfidf__lowercase": True,
            "tfidf__stop_words": stop_words,
            "tfidf__ngram_range": (1, 2),
            "tfidf__max_df": 0.5,
            "tfidf__min_df": 0.04,
            "tfidf__max_features": 125,
            # "tfidf__input": "content",
            # "tfidf__decode_error": "strict",
            "tfidf__preprocessor": preprocess,
            # "tfidf__tokenizer": None,
            # "tfidf__analyzer": "word",
            # "tfidf__token_pattern": "(?u)\\b\\w\\w+\\b",
            "lda__n_components": 10,
            "lda__max_iter": 15,
            "lda__random_state": 0,
            "lda__learning_method": "online",
            "lda__verbose": True,
        }
        pipeline.set_params(**pipeline_params)

        # Learn model
        tic = datetime.datetime.now()
        X_transformed = pipeline.fit_transform(X=df["doc"].values)
        toc = datetime.datetime.now()
        pipeline_time = (toc - tic).seconds
        print(f"Pipeline fit in {pipeline_time} seconds")

        # Get topic keywords array & save
        print("Extracting Topics...")
        topics = get_top_keywords_per_topic(
            pipeline.named_steps["tfidf"], pipeline.named_steps["lda"], top_n=7
        )

        pd.DataFrame({"topics": topics.tolist()}).to_csv(KEYWORDS_PER_TOPIC_FILE)
        # np.savetxt(fname="topics.txt", X=topics, delimiter=",", fmt="%s")

        # Get topics per doc & join to data df
        top_n = 3
        results = get_top_topics_per_doc(X_transformed, top_n=top_n)
        df = df.merge(
            pd.DataFrame(
                results["top_topic_inds"],
                columns=[f"topic_ind_{i}" for i in range(top_n)],
            ),
            left_index=True,
            right_index=True,
        )

        df = df.merge(
            pd.DataFrame(
                results["top_topic_weights"],
                columns=[f"topic_weight_{i}" for i in range(top_n)],
            ),
            left_index=True,
            right_index=True,
        )

        # Write dataframe
        df.to_csv(DATAFRAME_FILE)

        # Write Tfidf features
        pd.Series(
            pipeline.named_steps["tfidf"].get_feature_names()
        ).sort_values().to_csv(TFIDF_FEATURES_FILE, index=False, header=False)

        # MLflow logging
        print("MLflow logging...")
        # mlflow.log_param("n_rows", n_rows)
        mlflow.log_metric("data_load_time", load_time)
        mlflow.log_metric("pipeline_time", pipeline_time)
        pipeline_params.pop("tfidf__stop_words")
        mlflow.log_params(pipeline_params)
        mlflow.sklearn.log_model(pipeline, "sk_models")
        mlflow.log_artifact(KEYWORDS_PER_TOPIC_FILE)
        mlflow.log_artifact(DATAFRAME_FILE)
        mlflow.log_artifact(TFIDF_FEATURES_FILE)
        client.set_tag(run_id=run.info.run_id, key="framework", value=framework)
        client.set_tag(
            run_id=run.info.run_id,
            key="preprocessing_version",
            value=preprocessing_version,
        )
        client.set_tag(
            run_id=run.info.run_id, key="stopwords_version", value=stopwords_version
        )

