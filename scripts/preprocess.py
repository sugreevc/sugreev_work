"""General text preprocessing"""


def lower(text: str):
    return text.lower()


# Define functions for stopwords, bigrams, trigrams and lemmatization,yield list of docs


def remove_stopwords(doc):
    # remove accents, keep words 2-15 chars, lower, remove stopwords
    # applied to list of chat lines
    return [
        word
        for word in simple_preprocess(" ".join(doc), deacc=True)
        if word not in stop_words
    ]


def lemmatization(docs, allowed_postags=["NOUN", "ADJ", "VERB", "ADV", "PROPN"]):
    """https://spacy.io/api/annotation"""
    docs_out = []
    for sent in docs:
        doc = nlp(" ".join(sent))
        docs_out.append(
            [token.lemma_ for token in doc if token.pos_ in allowed_postags]
        )
    return docs_out
