"""Feature extraction methods for time series data"""

import numpy as np
import pandas as pd

from sklearn.base import BaseEstimator, TransformerMixin
from sklearn.preprocessing import FunctionTransformer
from sklearn.utils.validation import check_X_y, check_array, check_is_fitted

from typing import Dict


def df_feature_extract(df: pd.DataFrame) -> pd.DataFrame:
    """
    Pivot longform quote data into producer quote timeseries,
    extract features and return in a dataframe.
    """
    tsdf = (
        df.drop("activation_week", axis=1)
        .pivot(index="producer_code", columns="quote_week", values="n_quotes")
        .fillna(0)
        .copy()
    )

    # Stats
    print(
        f"{df.producer_code.nunique()} unique producers quoting over \
            {df.quote_week.nunique()} weeks, {df.quote_week.min().date()} to {df.quote_week.max().date()}"
    )

    a_week = df.groupby("producer_code")["activation_week"].agg("first")
    q_week = df.groupby("producer_code").apply(lambda x: x["quote_week"].min())

    # Extract features
    feature_list = []
    for pc, ts in tsdf.iterrows():
        feature_list.append(_ts_feature_extract(pc, ts, a_week, q_week))

    return pd.DataFrame(feature_list)


# TODO: look at tsfresh https://github.com/blue-yonder/tsfresh
def _ts_feature_extract(
    producer_code: str, ts: pd.Series, a_week: pd.Timestamp, q_week: pd.Timestamp
) -> Dict:
    """
    Applied to each row (timeseries of a single producer) of a
    timeseries dataframe.
    """

    # Trim each producer time series to start at first week of quote or activation
    start_week = min(a_week[producer_code], q_week[producer_code])
    trimmed = ts.loc[start_week:]

    nweeks_active = trimmed[trimmed > 0].size
    weeks_active_index = trimmed[trimmed > 0].index
    nweeks = trimmed.size
    ndays = nweeks * 7.0
    nquotes = trimmed.sum()

    fraction_weeks_active = nweeks_active / nweeks

    out_keys = [
        "producer_code",
        "fraction_weeks_active_scaled",
        "fraction_active_period_scaled",
        "ip_min_scaled",
        "ip_max_scaled",
        "ip_mean_scaled",
        "ip_std_scaled",
        "when_activated_scaled",
        "ip_mean",
        "ip_std",
        "nquotes",
    ]
    if fraction_weeks_active == 0:
        out_vals = [
            producer_code,
            fraction_weeks_active,
            0,
            1,
            1,
            1,
            1,
            0,
            None,
            None,
            nquotes,
        ]
        return dict(zip(out_keys, out_vals))

    # get number of days between quotes and days from last quote until end
    inactive_periods = np.append(
        [
            interval.days
            for interval in (weeks_active_index[1:] - weeks_active_index[:-1])
        ],
        (trimmed.index[-1] - weeks_active_index[-1]).days,
    )

    # days from first to last active
    active_period = (weeks_active_index[-1] - weeks_active_index[0]).days

    #     #churn event if more than a week stop after last active
    #     churn = False
    #     if inactive_periods[-1]>=10:
    #         churn = True

    # min/max/mean/std quotes per active week
    nquotes_weekly_stats = (
        trimmed[trimmed > 0].agg(["min", "max", "mean", "std"]).fillna(0).values
        / nquotes
    )

    # fraction through cycle where activation occurs
    try:
        when_activated = trimmed.index.get_loc(a_week[producer_code]) / nweeks
    except:
        when_activated = 0

    # fraction of quotes per quintile
    bounds = np.linspace(0, nweeks, 6, dtype=int)
    quintile_fractions = [
        trimmed[bounds[i] : bounds[i + 1]].sum() / nquotes
        for i in range(bounds.size - 1)
    ]

    out_vals = [
        producer_code,
        fraction_weeks_active,
        active_period / ndays,
        inactive_periods.min() / ndays,
        inactive_periods.max() / ndays,
        inactive_periods.mean() / ndays,
        inactive_periods.std() / ndays,
        when_activated,
        inactive_periods.mean(),
        inactive_periods.std(),
        nquotes,
    ]

    features = dict(zip(out_keys, out_vals))

    features.update(
        dict(
            zip(
                [
                    "nq_week_min_scaled",
                    "nq_week_max_scaled",
                    "nq_week_mean_scaled",
                    "nq_week_std_scaled",
                ],
                nquotes_weekly_stats,
            )
        )
    )
    features.update(
        dict(
            zip(
                [
                    "q1_quotes_scaled",
                    "q2_quotes_scaled",
                    "q3_quotes_scaled",
                    "q4_quotes_scaled",
                    "q5_quotes_scaled",
                ],
                quintile_fractions,
            )
        )
    )

    return features


feature_extraction_transformer = FunctionTransformer(func=df_feature_extract)