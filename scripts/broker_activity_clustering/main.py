import datetime
from collections import defaultdict

import pandas as pd
import numpy as np

import mlflow
import mlflow.sklearn
from mlflow.tracking import MlflowClient

from sklearn.pipeline import Pipeline, FeatureUnion
from sklearn.cluster import KMeans
from sklearn.manifold import TSNE

from time_series_feature_extraction import feature_extraction_transformer
from util import (
    load_quote_data,
    get_data_path,
    cluster_features_describe,
    calc_cluster_cardinality,
)


if __name__ == "__main__":

    # Set experiment
    experiment_name = "2019_quotes"
    mlflow.set_experiment(experiment_name)
    client = MlflowClient()
    experiment_id = client.get_experiment_by_name(experiment_name).experiment_id

    # define datafile
    data_filename = "quotes_producer_week.csv"

    # Define output filenames
    DATAFRAME_FILE = "features_dataframe.csv"
    CLUSTER_SUMMARY_FILENAME = "cluster_summary.csv"

    for feature_set in [
        # ["ip_mean_scaled"],
        # ["nq_mean_scaled"],
        # ["fraction_weeks_active_scaled"],
        # ["ip_mean_scaled", "nq_mean_scaled", "fraction_weeks_active_scaled"],
        []
    ]:
        with mlflow.start_run(experiment_id=experiment_id) as run:

            print(
                f"Started MLflow run, experiment {experiment_id}, run {run.info.run_id}"
            )

            # Load Data
            df = load_quote_data(get_data_path(data_filename=data_filename))

            fdf = feature_extraction_transformer.transform(df)
            # TODO: this and feature extraction should be combined into one or two
            # transfomers for a pipeline; first to make tsdf, second to extract features;
            # The second transformer will have 2 steps: 1 to extract un-normalized features
            # and 2 to preprocess/normalize

            feature_cols = [f for f in fdf.columns.tolist() if f != "producer_code"]
            scaled_feature_cols = [f for f in fdf.columns.tolist() if "scaled" in f]

            # Set MLFlow tags
            mlflow_features_tag = ",".join(feature_set) if feature_set else "all"

            # Only keep scaled features for k-means
            cluster_cols = feature_set or scaled_feature_cols
            X = fdf[cluster_cols].values

            # Perform kmeans, save cluster metrics
            cluster_inertia = defaultdict(float)
            K = range(3, 6)

            for k in K:
                tic = datetime.datetime.now()

                km = KMeans(n_clusters=k, random_state=42, n_init=30, max_iter=400)
                km.fit(X)
                k_key = "k" + str(k)
                cluster_inertia[k_key] = km.inertia_
                fdf[k_key] = km.predict(X)

                # Cluster cardinality, feature summary
                kdf = pd.concat(
                    [
                        calc_cluster_cardinality(fdf, k_key),
                        cluster_features_describe(fdf, k_key, "mean", feature_cols),
                        cluster_features_describe(fdf, k_key, "std", feature_cols),
                    ],
                    axis=1,
                )

                kfile = "_".join([k_key, CLUSTER_SUMMARY_FILENAME])
                kdf.to_csv(kfile)
                mlflow.log_artifact(kfile)
                # save sample heatmaps

                toc = datetime.datetime.now()
                kmeans_time = (toc - tic).seconds
                print(f"kmeans {k} done! Time elapsed: {kmeans_time} seconds")

            # plot elbow and save
            # plt.plot(K, Sum_of_squared_distances, "bx-")
            # plt.xlabel("k")
            # plt.ylabel("Sum_of_squared_distances")
            # plt.title("Elbow Method For Optimal k")
            # plt.show()
            # plt.save()

            # t-SNE
            # time_start = time.time()
            # tsne = TSNE(n_components=2, verbose=1, n_iter=1600, perplexity=30)
            # tsne_results = tsne.fit_transform(X)
            # print("t-SNE done! Time elapsed: {} seconds".format(time.time() - time_start))

            # fdf["tsne1"] = tsne_results[:, 0]
            # fdf["tsne2"] = tsne_results[:, 1]

            # Plot tSNE + K-means
            # _, ax = plt.subplots(figsize=(10, 7.3))
            # sns.scatterplot(
            #     x="tsne1",
            #     y="tsne2",
            #     hue="k5",
            #     palette=sns.color_palette("hls", 5),
            #     data=fdf,
            #     #     legend="full",
            #     alpha=0.3,
            #     ax=ax,
            # )
            # ax.legend()

            # Setup Feature Union
            # feature_union = FeatureUnion([("kmeans", km), ("tsne", tsne)])

            # # Setup pipeline
            # pipeline = Pipeline(
            #     [("tfidf", TfidfVectorizer()), ("lda", LatentDirichletAllocation())]
            # )

            # pipeline_params = {
            #     "tfidf__strip_accents": "unicode",
            #     "tfidf__lowercase": True,
            #     "tfidf__stop_words": stop_words,
            #     "tfidf__ngram_range": (1, 2),
            #     "tfidf__max_df": 0.5,
            #     "tfidf__min_df": 0.04,
            #     "tfidf__max_features": 125,
            #     # "tfidf__input": "content",
            #     # "tfidf__decode_error": "strict",
            #     "tfidf__preprocessor": preprocess,
            #     # "tfidf__tokenizer": None,
            #     # "tfidf__analyzer": "word",
            #     # "tfidf__token_pattern": "(?u)\\b\\w\\w+\\b",
            #     "lda__n_components": 10,
            #     "lda__max_iter": 15,
            #     "lda__random_state": 0,
            #     "lda__learning_method": "online",
            #     "lda__verbose": True,
            # }
            # pipeline.set_params(**pipeline_params)

            # Learn model
            # tic = datetime.datetime.now()
            # X_transformed = pipeline.fit_transform(X=df["doc"].values)
            # toc = datetime.datetime.now()
            # pipeline_time = (toc - tic).seconds
            # print(f"Pipeline fit in {pipeline_time} seconds")

            # # Get topic keywords array & save
            # print("Extracting Topics...")
            # topics = get_top_keywords_per_topic(
            #     pipeline.named_steps["tfidf"], pipeline.named_steps["lda"], top_n=7
            # )

            # pd.DataFrame({"topics": topics.tolist()}).to_csv(KEYWORDS_PER_TOPIC_FILE)
            # # np.savetxt(fname="topics.txt", X=topics, delimiter=",", fmt="%s")

            # # Get topics per doc & join to data df
            # top_n = 3
            # results = get_top_topics_per_doc(X_transformed, top_n=top_n)
            # df = df.merge(
            #     pd.DataFrame(
            #         results["top_topic_inds"],
            #         columns=[f"topic_ind_{i}" for i in range(top_n)],
            #     ),
            #     left_index=True,
            #     right_index=True,
            # )

            # df = df.merge(
            #     pd.DataFrame(
            #         results["top_topic_weights"],
            #         columns=[f"topic_weight_{i}" for i in range(top_n)],
            #     ),
            #     left_index=True,
            #     right_index=True,
            # )

            # Write features,labels dataframe
            fdf.to_csv(DATAFRAME_FILE, index=False)

            # Write Tfidf features
            # pd.Series(
            #     pipeline.named_steps["tfidf"].get_feature_names()
            # ).sort_values().to_csv(TFIDF_FEATURES_FILE, index=False, header=False)

            # MLflow logging
            print("MLflow logging...")
            # mlflow.log_metric("data_load_time", load_time)
            mlflow.log_metric("kmeans_time", kmeans_time)
            mlflow.log_param("data_source", data_filename)
            # mlflow.log_params(pipeline_params)
            # mlflow.sklearn.log_model(pipeline, "sk_models")
            mlflow.log_artifact(DATAFRAME_FILE)
            client.set_tag(
                run_id=run.info.run_id, key="features", value=mlflow_features_tag
            )
