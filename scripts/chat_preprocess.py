"""Preprocessing of text data for topic analysis"""

import spacy

# !python -m spacy download en_core_web_md
# !python -m spacy link en_core_web_md en --force


nlp = spacy.load("en", disable=["ner", "par"])


def lower_stop_punct_lemma(chat_text, stop_words, first_n_lines=3):
    """processes list of customer chats (first n lines)
    and returns unique list of lemmatized tokens.
    https://spacy.io/api/annotation

    
    Args:
        chat_text (list(str) or str): new broker query or list of old ticket broker chat lines
        stop_words (set): stopwords to remove from chat_text
        first_n_lines (int, optional): if chat_text is a list, how many to consider for processing

    Returns:
        spacy doc: cleaned chat text 

    """

    doc_out = []

    if isinstance(chat_text, list):
        doc = nlp(" ".join(chat_text[:first_n_lines]).lower())
    elif isinstance(chat_text, str):
        doc = nlp(chat_text.lower())
    else:
        raise TypeError("Should be list(str) or str")

    for token in doc:
        if not (token.is_punct or token.is_stop):
            lemma = token.lemma_
            if (lemma not in stop_words) and (lemma not in doc_out):
                doc_out.append(lemma)
    return " ".join(doc_out)
