"""Get list of CCC names """


def make_ccc_agent_first_names_list(agent_names):
    """Takes list of unique assignee names from zendesk ticket data and returns 
    list of first names, lowered for used in stopwords."""

    return [n.split()[0].lower() for n in agent_names]
