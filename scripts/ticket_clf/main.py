import datetime

import pandas as pd

import mlflow
import mlflow.sklearn
from mlflow.tracking import MlflowClient

from sklearn.model_selection import train_test_split
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.linear_model import LogisticRegression
from sklearn.pipeline import Pipeline

from sklearn.model_selection import (
    StratifiedKFold,
    RepeatedStratifiedKFold,
    cross_validate,
    cross_val_score,
)
from sklearn.metrics import (
    classification_report,
    balanced_accuracy_score,
    confusion_matrix,
)

from stopwords import get_stopwords
from util import load_data, get_data_path
from email_preprocess import preprocess


if __name__ == "__main__":

    # Set experiment
    experiment_name = "multiclass_clf_dev"
    mlflow.set_experiment(experiment_name)
    client = MlflowClient()
    experiment_id = client.get_experiment_by_name(experiment_name).experiment_id

    # define datafile
    data_filename = "email_web_text_labeled_2021-01.csv"

    # Set MLFlow tags
    framework = "sklearn"
    preprocessing_version = "v2"
    stopwords_version = "v2"

    stop_words = get_stopwords()

    # Define output filenames
    TFIDF_FEATURES_FILE = "tfidf_features.csv"

    # Set constants
    RANDOM_STATE = 42

    with mlflow.start_run(experiment_id=experiment_id) as run:

        print(f"Started MLflow run, experiment {experiment_id}, run {run.info.run_id}")

        # Load Data
        df = load_data(get_data_path(data_filename=data_filename))

        # Split to train/dev, test
        X = df["text"].values
        y = df["label"].values

        X_train_dev, X_test, y_train_dev, y_test = train_test_split(
            X, y, train_size=0.9, random_state=RANDOM_STATE, stratify=y
        )

        # Setup pipeline
        pipeline = Pipeline(
            [("tfidf", TfidfVectorizer()), ("lr", LogisticRegression())]
        )

        pipeline_params = {
            "tfidf__strip_accents": "unicode",
            "tfidf__lowercase": True,
            "tfidf__stop_words": stop_words,
            "tfidf__ngram_range": (1, 2),
            "tfidf__max_df": 0.5,
            "tfidf__min_df": 0.03,
            "tfidf__max_features": 100,
            # "tfidf__input": "content",
            # "tfidf__decode_error": "strict",
            "tfidf__preprocessor": preprocess,
            # "tfidf__tokenizer": None,
            # "tfidf__analyzer": "word",
            # "tfidf__token_pattern": "(?u)\\b\\w\\w+\\b",
            "lr__multi_class": "ovr",
            "lr__solver": "saga",
            "lr__verbose": 1,
            "lr__n_jobs": 4,
        }
        pipeline.set_params(**pipeline_params)

        # Learn model
        tic = datetime.datetime.now()
        pipeline.fit(X=X_train_dev, y=y_train_dev)
        toc = datetime.datetime.now()
        pipeline_time = (toc - tic).seconds
        print(f"Pipeline fit in {pipeline_time} seconds")

        # Score
        y_pred = pipeline.predict(X_test)
        bas = balanced_accuracy_score(y_test, y_pred)
        print(f"Balanced accuracy score: {bas}")
        cr = classification_report(y_test, y_pred, output_dict=True)
        print(f"Classfication report: {cr}")

        # Write dataframe
        # df.to_csv(DATAFRAME_FILE)

        # Write Tfidf features
        pd.Series(
            pipeline.named_steps["tfidf"].get_feature_names()
        ).sort_values().to_csv(TFIDF_FEATURES_FILE, index=False, header=False)

        # MLflow logging
        print("MLflow logging...")
        # mlflow.log_param("n_rows", n_rows)
        mlflow.log_metric("balanced_accuracy_score", bas)
        mlflow.log_metric("pipeline_time", pipeline_time)
        pipeline_params.pop("tfidf__stop_words")
        mlflow.log_params(pipeline_params)
        # mlflow.sklearn.log_model(pipeline, "sk_models")
        # mlflow.log_artifact(DATAFRAME_FILE)
        mlflow.log_artifact(TFIDF_FEATURES_FILE)
        client.set_tag(run_id=run.info.run_id, key="framework", value=framework)
        client.set_tag(
            run_id=run.info.run_id,
            key="preprocessing_version",
            value=preprocessing_version,
        )
        client.set_tag(
            run_id=run.info.run_id, key="stopwords_version", value=stopwords_version
        )
