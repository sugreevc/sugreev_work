email_web_text_labeled_2021-01.csv

See notebook "2021-01_ticket_labels.ipynb" for filters and query used to retrieve this data. Besides 
filters used to remove internal and automated emails, this CSV only contains ticket IDS hand-labeled 
with the method shown in the notebook. Data is > 2019-01-01, ~40k tickets. 4 cols: ticket ID, timestamp 
created, text, label.