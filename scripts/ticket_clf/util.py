from pathlib import Path
from typing import Union
import numpy as np
import pandas as pd
from decorators import timer


PROJECT_ROOT = Path(
    "/Users/sugreevchawla/Desktop/repos/sugreev_work/scripts/ticket_clf"
)


def get_data_path(data_filename: str) -> Path:
    return PROJECT_ROOT / "data" / data_filename


# Load data
@timer
def load_data(data_path: Union[str, Path]) -> pd.DataFrame:
    df = pd.read_csv(
        filepath_or_buffer=data_path,
        header=0,  # ignore names in csv and replace with 'names' below
        names=("ticket_id", "t_created", "text", "label"),
        parse_dates=[1],
    )
    return df


# TFIDF inspection
def inspect_tfidf(fit_tfidf_vectorizer, vectorized_data):
    """
    Return all feature names and set of top 5 features for each doc

    Args:
        fit_tfidf_vectorizer (sklearn vectorizer): after executing vectorizer.fit()
        vectorized_data (numpy sparse matrix): result of vectorizer.transform()

    Returns:
        dict of
            feature_array (numpy array): all features from fit_tfidf_vectorizer.get_feature_names
            top_features (numpy array): get top features for each doc and return flattened unique elements
    """

    feature_array = np.array(fit_tfidf_vectorizer.get_feature_names())

    # top 5 most important words for every doc
    tfidf_sorting = np.ravel(
        np.fliplr(np.argsort(vectorized_data.toarray(), axis=1))[:, :5], order="F"
    )
    u, ind, cnt = np.unique(
        feature_array[tfidf_sorting], return_index=True, return_counts=True
    )

    return {
        "feature_names": feature_array,
        "top_features_counts": np.array(list(zip(u, cnt)))[np.argsort(ind)],
        "top_features": u[np.argsort(ind)],
    }


def find_docs_with_word(word, tfidf_vectorizer, vectorized_data, data, highlight=False):
    """
    Get doc indices of data matrix (n docs x n features) that contain
    nonzero feature weight for 'word'

    Useful for building stopwords lists; if a word appears in a topic
    or is highly weighted according to TFIDF, apply this function to find
    docs containing the word so you can manually decide importance.

    Args:
        word (str): word (decoded tfidf feature) of interest
        fit_tfidf_vectorizer (sklearn vectorizer): after executing vectorizer.fit()
        vectorized_data (numpy sparse matrix): result of vectorizer.transform()
        data (numpy array): text data fit_transformed by vectorizer, n docs x 1
        highlight (optional, bool): True if word should be highlighted when printed
            with ANSI escape chars

    Returns:
        doc_inds (numpy array)
    """

    feature_index = tfidf_vectorizer.vocabulary_[word]
    doc_inds = vectorized_data[:, feature_index].toarray().squeeze().nonzero()[0]
    if highlight:
        return np.array(list(map(lambda x: highlight_word(x, word), data[doc_inds])))
    return data[doc_inds]


def highlight_word(text, word):
    return text.lower().replace(word, "\x1b[1;03;30;43m" + word + "\x1b[0m")
