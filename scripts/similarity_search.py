# similarity search with direct word search (OR, AND) and topic similarity search
# code is for knn + cosine distance
# to be tested against with LDA model on new query + jensen shannon distance, and spacy similarity


# import spacy
# !python -m spacy download en_core_web_md
# !python -m spacy link en_core_web_md en --force
# nlp = spacy.load("en", disable=["ner", "par"])

from sklearn.neighbors import NearestNeighbors
from vectorize import get_tfidf_vectorizer


# Using spacy vector embeddings and similarity
from text_processing import lower_stop_punct_lemma


def process_query(query, stopwords):
    """processes incoming broker query

    
    Args:
        query (str): new broker query,
        stopwords (set): set of stopwords

    Returns:
        spacy doc: cleaned chat text 

    """

    return lower_stop_punct_lemma(query, stopwords)


def get_similar_tickets(query_doc, df, top_n=5):
    """processes list of customer chats (first n lines)
    and returns unique list of lemmatized tokens.
    https://spacy.io/api/annotation

    
    Args:
        query_doc (spacy doc): new broker query, processed and transformed to spacy doc
        df (pandas DataFrame): dataframe containing previous ticket data for similarity search, 
        requires columns 'cleaned_customer_chats', 't_id', 'comment_text'
        first_n_lines (int, optional): if chat_text is a list, how many to consider for processing

    Returns:
        spacy doc: cleaned chat text 

    """
    results = []

    top_scores_series = (
        df["cleaned_customer_chats"]
        .map(lambda x: x.similarity(query_doc))
        .sort_values(ascending=False)[:top_n]
    )

    for i, row in df.loc[top_scores_series.index].iterrows():
        results.append(
            [
                row["t_id"],
                row["t_created"],
                row["comment_text"],
                top_scores_series.loc[i],
            ]
        )

    return results


def display_similar_ticket_results(results):
    for t in results:
        print(
            "Ticket ID: {}, Ticket Date: {}, Similarity Score: {}\n".format(
                t[0], t[1], round(t[3], 3)
            )
        )
        print(t[2])


# TFIDF + KNN-cosine
def get_similar_tickets_knn():
    tfidf = get_tfidf_vectorizer()
    tfv = tfidf.fit_transform(data)

    nn = NearestNeighbors(metric="cosine", algorithm="brute")
    nn.fit(tfv)

    dist, inds = nn.kneighbors(tfv[3])
    for i, d in enumerate(dist[0]):
        print("--------------------" + " Distance: {:.3f}".format(d))
        text = merged_chat_ticket_processing.comment_text.iloc[inds[0][i]]
        for keyword in top_n_tfidf_features[inds[0][i]]:
            text = text.replace(keyword, "\x1b[1;03;30;43m" + keyword + "\x1b[0m")
        print(text)

