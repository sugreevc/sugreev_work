{
 "cells": [
  {
   "cell_type": "raw",
   "metadata": {},
   "source": [
    "---\n",
    "title: Designing an AB test for Rewards Engagement\n",
    "authors:\n",
    "- Sugreev Chawla\n",
    "tags:\n",
    "- hypothesis_testing\n",
    "- ab_test\n",
    "- sampling algorithms\n",
    "created_at: 2021-01-05\n",
    "tldr: This post describes how to calculate the minimum observable effect size given a desired alpha (type I error) and power (1 - type II error) level for an AB test, using the new portal invoice page experiment from 7/2020 as an example.\n",
    "---"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Motivation\n",
    "\n",
    "Experimental design is often constrained by one or all of:  \n",
    "- the number of data samples that can be collected.  \n",
    "- the maximum amount of time an experiment can be run before a decision needs to be made.  \n",
    "- the minimum statistically significant effect size one can resolve for a particular business case.\n",
    "\n",
    "As an example, we consider the AB test of the new invoice page launched the week of 7/27/2020. The new page was deployed to a treatment group of ~ 430 producer codes, and a control group of a similar number of producers was monitored for comparison. The metric of interest was \n",
    "`# of billing tickets / # of Amplitude sessions with invoice page view`. \n",
    "\n",
    "Only billing tickets with small set of tags relevant to invoice page issues were considered. The goal of the experiment was to determine what effect the new page design had on ticket volume. From previous analysis, we knew that the test group sizes we chose would yield ~1k Amplitude sessions per week. Relevant questions are then:  \n",
    "- How long do we need to run this experiment to be able to see a 10% change in conversion rate? ( `conversion_rate = num_tickets / num_sessions`).  \n",
    "- If we need to make a decision at the end of 2 months, what's the minimum effect size we can resolve for a particular power level? (Note effect size here is the standardized effect size described in docs linked below, which needs to be translated into a difference of conversion rates or proportions)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Calculating effect size\n",
    "\n",
    "The following methods help calculate the minimum effect size observable given desired alpha (type I error), power (1 - type II) levels and control/treatment sample sizes using a z-test. (see [zt_ind_solve_power](https://www.statsmodels.org/stable/generated/statsmodels.stats.power.zt_ind_solve_power.html#statsmodels.stats.power.zt_ind_solve_power)  and [proportion_effectsize](https://www.statsmodels.org/stable/generated/statsmodels.stats.proportion.proportion_effectsize.html#statsmodels.stats.proportion.proportion_effectsize) for details).  \n",
    "- Type I error is defined as the rate of false positives, i.e. if alpha=0.05 then we expect to incorrectly conclude a signifcant difference between the control & treatment groups <=5% of the time, assuming there is no difference.  \n",
    "- Type II error is the rate of false negatives; if power=0.95 then we expect to incorrectly conclude the treatment & control groups are the same <=5% of the time even though they are different."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 69,
   "metadata": {
    "code_folding": []
   },
   "outputs": [],
   "source": [
    "import statsmodels.stats.api as st\n",
    "\n",
    "def calc_min_effect_size(control_n=5000, treatment_n=5000, power=0.95, alpha=0.05):\n",
    "    '''Use statsmodels z-test to solve for standardized effect size.'''\n",
    "    ratio = treatment_n/control_n\n",
    "    es = st.zt_ind_solve_power(\n",
    "        effect_size=None, nobs1=control_n, power=power, alpha=alpha, ratio=ratio)\n",
    "    return es\n",
    "\n",
    "def calc_proportion_effectsize(treatment_prop, control_prop):\n",
    "    '''Translates difference in observed proportions to a standardized effect size.'''\n",
    "    return st.proportion_effectsize(treatment_prop, control_prop)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Discussion"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Question 1"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "1) A 10% increase in conversion rate implies a treatment group rate of 2.75% (assuming baseline conversion rate of 2.5%), which translates to an effect size of 0.0156:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 118,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "0.01564129472902903"
      ]
     },
     "execution_count": 118,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "calc_proportion_effectsize(.0275, .025)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Scanning a range of possible sample sizes shows we'd need around 107k samples to resolve this change."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 112,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Num control/treatment samples = 105000\n",
      "Minimum observable effect size:  0.0157\n",
      "Num control/treatment samples = 106000\n",
      "Minimum observable effect size:  0.0157\n",
      "Num control/treatment samples = 107000\n",
      "Minimum observable effect size:  0.0156\n",
      "Num control/treatment samples = 108000\n",
      "Minimum observable effect size:  0.0155\n",
      "Num control/treatment samples = 109000\n",
      "Minimum observable effect size:  0.0154\n"
     ]
    }
   ],
   "source": [
    "power = 0.95\n",
    "alpha = 0.05\n",
    "for n_samp in range(105000, 110000, 1000):\n",
    "    print(f\"Num control/treatment samples = {n_samp}\")\n",
    "    print(f\"Minimum observable effect size: \"\n",
    "          f\" {calc_min_effect_size(control_n=n_samp, treatment_n=n_samp):.4f}\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "If our conversion rate decreases by 10%, the same calculation yields 97k samples."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Question 2"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "2) If we stop our experiment after 2 months (8k samples at 1k samples/week), we will be able to resolve an effect size of 0.057:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 125,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "0.05699721386533439"
      ]
     },
     "execution_count": 125,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "n_samp = 8000\n",
    "calc_min_effect_size(control_n=n_samp, treatment_n=n_samp, power=0.95)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This translates to an observed treatment conversion rate of (again assuming 2.5% baseline conversion) of ~3.5% if there is an increase:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 116,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Treatment: 0.030 Standard Effect Size: 0.031\n",
      "Treatment: 0.031 Standard Effect Size: 0.036\n",
      "Treatment: 0.032 Standard Effect Size: 0.042\n",
      "Treatment: 0.033 Standard Effect Size: 0.048\n",
      "Treatment: 0.034 Standard Effect Size: 0.053\n",
      "Treatment: 0.035 Standard Effect Size: 0.059\n"
     ]
    }
   ],
   "source": [
    "import numpy as np\n",
    "control_conversion_rate = 0.025\n",
    "for treatment_conversion_rate in np.arange(.03, .035, .001):\n",
    "    print(f\"Treatment: {treatment_conversion_rate:.03f} \"\n",
    "          f\"Standard Effect Size: \"\n",
    "          f\"{calc_proportion_effectsize(treatment_conversion_rate, control_conversion_rate):.3f}\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "And ~1.7% if there is a decrease:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 124,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Treatment: 0.015 Standard Effect Size: -0.072\n",
      "Treatment: 0.016 Standard Effect Size: -0.064\n",
      "Treatment: 0.017 Standard Effect Size: -0.056\n",
      "Treatment: 0.018 Standard Effect Size: -0.048\n",
      "Treatment: 0.019 Standard Effect Size: -0.041\n",
      "Treatment: 0.020 Standard Effect Size: -0.034\n"
     ]
    }
   ],
   "source": [
    "import numpy as np\n",
    "control_conversion_rate = 0.025\n",
    "for treatment_conversion_rate in np.arange(.015, .02, .001):\n",
    "    print(f\"Treatment: {treatment_conversion_rate:.03f} \"\n",
    "          f\"Standard Effect Size: \"\n",
    "          f\"{calc_proportion_effectsize(treatment_conversion_rate, control_conversion_rate):.3f}\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "However, if we were to accept a lower statistical power of 0.8, we could resolve an effect size of .044, or a treatment conversion rate of 1.8-1.9%."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 128,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "0.0442969237229905"
      ]
     },
     "execution_count": 128,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "n_samp = 8000\n",
    "calc_min_effect_size(control_n=n_samp, treatment_n=n_samp, power=0.8)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Invoice Page AB Test"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "For the invoice page AB test, a [Looker dashboard](https://attune.looker.com/dashboards/230) was set up to track conversion rates and the running treatment effect of the new invoice page design on billing tickets. Notice that after 2 months (Row 8 of the first table \"Running Statistics\", 2020-09-14), we had 6424 (6957) samples in the control (treatment) groups). The minimum effect size we can resolve is then 0.056:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 87,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "0.05608901258867613"
      ]
     },
     "execution_count": 87,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "calc_min_effect_size(control_n=6424, treatment_n=6957, power=power)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This effect size translates to a ~3.44% conversion rate for the treatment group:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 95,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "0.056185810736605724"
      ]
     },
     "execution_count": 95,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "calc_proportion_effectsize(.0344, .0249)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "But what we observed was a 2.64% treatment conversion rate, which translates to an effect size well below the minimum of 0.056. Thus our observed change in the treatment group cannont be concluded to be significant."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 96,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "0.009489333830102531"
      ]
     },
     "execution_count": 96,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "calc_proportion_effectsize(.0264, .0249)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Appendix"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We can more quickly explore the dependence of treatment effect on alpha, power, sample size in Jupyter notebooks using interactive widgets. Here, we use [ipywidges interact](https://ipywidgets.readthedocs.io/en/latest/examples/Using%20Interact.html#Fixing-arguments-using-fixed) to set up interactive sliders. (This won't render in knowledge repo unfortunately)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 21,
   "metadata": {},
   "outputs": [],
   "source": [
    "from ipywidgets import interact, fixed\n",
    "\n",
    "interact(calc_min_effect_size, \n",
    "         control_n=(5000,10000,500),\n",
    "         treatment_n=(5000,10000,500),\n",
    "         power=fixed(.95),\n",
    "         alpha=fixed(0.5),\n",
    "         verbose=fixed(True)\n",
    "        );"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "hide_input": false,
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.9.1"
  },
  "toc": {
   "base_numbering": 1,
   "nav_menu": {},
   "number_sections": true,
   "sideBar": true,
   "skip_h1_title": false,
   "title_cell": "Table of Contents",
   "title_sidebar": "Contents",
   "toc_cell": false,
   "toc_position": {},
   "toc_section_display": true,
   "toc_window_display": true
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
