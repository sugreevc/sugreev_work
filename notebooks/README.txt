Notebook Descriptions
---------------------

ticket_timeseries - Timeseries plots of all tickets over time, broken down by inquiry type
ticket_times - merge chats on ticket ID, splitting over 5 minute gaps, explore long tickets
