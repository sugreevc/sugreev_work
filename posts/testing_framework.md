## Testing

### VSCode
Specific tutorials to go over are Linting & [Testing](https://code.visualstudio.com/docs/python/testing#_enable-a-test-framework).  
- Since we’re using Pytest, make sure to enable the pytest framework in your settings.  
- For linters, I’m currently using Pylance, a new release by Microsoft for Python. You'll need to get a VSCode extension.


### Pytest

For those that haven't used Pytest before, [this](https://realpython.com/pytest-python-testing/) is a good resource describing it's main features as compared to unittest. 

To summarize the first article, Pytest highlights are:  
- Modularized [fixtures](https://docs.pytest.org/en/stable/fixture.html#teardown-cleanup-aka-fixture-finalization) for test dependencies
- Filter tests to run based on name or Mark (essentially a custom test tag)
- Parameterization of tests to separate test behavior from test data
- Durations for N slowest tests exposted

[Suggested test directory structure](https://docs.pytest.org/en/reorganize-docs/new-docs/user/directory_structure.html)

For advanced pytest-ing tips from a pandas library contributor, check [this](https://levelup.gitconnected.com/advanced-pytest-techniques-i-learned-while-contributing-to-pandas-7ba1465b65eb) out.

Additional packages: pytest-randomly to uncover state dependent tests, pytest-cov for coverage

